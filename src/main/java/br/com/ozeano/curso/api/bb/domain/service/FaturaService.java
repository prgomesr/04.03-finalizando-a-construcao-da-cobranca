package br.com.ozeano.curso.api.bb.domain.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ozeano.curso.api.bb.domain.model.Fatura;
import br.com.ozeano.curso.api.bb.domain.repository.FaturaRepository;
import br.com.ozeano.curso.api.bb.infra.model.input.CobrancaInput;

@Service
public class FaturaService {

	@Autowired
	private FaturaRepository repository;

	public CobrancaInput transformarFaturaEmCobranca(Long faturaId) {
		var fatura = repository.getOne(faturaId);

		return criar(fatura);
	}

	public CobrancaInput criar(Fatura fatura) {

		var builder = CobrancaInput.builder();

		builder.numeroConvenio(Long.valueOf(fatura.getConvenio().getNumeroContrato()));
		builder.numeroCarteira(Integer.valueOf(fatura.getConvenio().getCarteira()));
		builder.numeroVariacaoCarteira(Integer.valueOf(fatura.getConvenio().getVariacaoCarteira()));
		builder.dataVencimento(converterData(fatura.getDataVencimento()));
		builder.dataEmissao(converterData(LocalDate.now()));
		builder.valorOriginal(fatura.getValor());
		builder.indicadorAceiteTituloVencido("S"); // S/N
		builder.codigoAceite('N');
		builder.codigoTipoTitulo(2);
		builder.descricaoTipoTitulo("Duplicata Mercantil");
		builder.indicadorPermissaoRecebimentoParcial('N');
		builder.numeroTituloBeneficiario(fatura.getNumeroDocumento());
		builder.numeroTituloCliente(criarNossoNumero(fatura));
		builder.indicadorPix("S");

		return builder.build();
	}

	private String converterData(LocalDate data) {
		return data.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
	}

	private String criarNossoNumero(Fatura fatura) {
		// regra: "000" + numero contrato convenio + 10 algarismos com zeros a esquerda
		// utilizar como 10 algarismos o numero documento

		return String.format("%010d", Long.valueOf(fatura.getConvenio().getNumeroContrato())).concat(String.format("%010d", Long.valueOf(fatura.getNumeroDocumento())));
	}
}
